# logged-hours

## Description
A python3 script that pulls logged hours from our NI&S Jira Cloud instance. There are many options such as filtering and output type.

## Installation
curl

## Usage
1. Get a token using the directions from https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/
1. Set up environemt variables
```
export JIRA_USER=<PID>@vt.edu
export JIRA_TOKEN=<your token>
export JIRA_PROJECT=<project key>
```
1. Install prerequisites
```
python3 -m pip install --upgrade --index-url https://code.vt.edu/api/v4/projects/20369/packages/pypi/simple --no-deps nis_jira_hours
```

### Examples
Get help output:
```
python3 -m nis_jira_hours -h
usage: nis_jira_hours.py [-h] [--csv] [--date DATE] [--start-date START_DATE] [--end-date END_DATE] [--days DAYS] [--seconds] [-u USER] [-v]

Retrieve hours logged per user and ticket from Jira Cloud. No arguments will output the previous business day.

options:
  -h, --help            show this help message and exit
  --csv                 Output in CSV format
  --date DATE           Specify the date in YYYY-MM-DD format (e.g., 2023-10-17)
  --start-date START_DATE
                        Specify the start date for the date range (YYYY-MM-DD)
  --end-date END_DATE   Specify the end date for the date range (YYYY-MM-DD)
  --days DAYS           Specify an integer for the number of past days to retreive
  --seconds             Output recorded time in seconds
  -u USER, --user USER  Specify the full or partial name to limit the results by (e.g., William)
  -v                    Verbose output
```

Get the last business day of work logs:
```
python3 -m nis_jira_hours
```

Get the last business day of work logs and filter for user:
```
python3 -m nis_jira_hours --user <subsring of user name>
```

Get entries for the last 30 days:
```
python3 -m nis_jira_hours --days 30
```

Get entries for a certain date:
```
python3 -m nis_jira_hours --date 2023-10-18
```

Get entries for a date range:
```
python3 -m nis_jira_hours --start-date 2023-10-01 --end-date 2023-10-18
```

Output as CSV with seconds for the unit of time:
```
python3 -m nis_jira_hours --csv --seconds
```

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
1. Clone the project
1. Run `pip3 install -r requirements.txt` to install dependencies
1. Run `python3 -m pip install --upgrade build`
1. Create a branch
1. Make your changes
1. Run the build script and test the binary
1. Push your branch upstream
1. Create a merge request