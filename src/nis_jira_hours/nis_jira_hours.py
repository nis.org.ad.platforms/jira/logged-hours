"""Module providing access to Jira worklog entries"""
import os
import argparse
from datetime import datetime, timedelta
import requests


def main():
    """Main function"""

    # Get Jira credentials and project key from environment variables
    username = os.environ.get("JIRA_USER")
    api_token = os.environ.get("JIRA_TOKEN")
    project_key = os.environ.get("JIRA_PROJECT")

    # Check if the required environment variables are set
    if not (username and api_token and project_key):
        print(
            "Please set the JIRA_USER, JIRA_TOKEN, and JIRA_PROJECT environment variables."
        )
        exit(1)

    # Parse command-line arguments
    parser = argparse.ArgumentParser(
        description="Retrieve hours logged per user and ticket from Jira Cloud. No arguments will\
        output the previous business day."
    )
    parser.add_argument("--csv", action="store_true", help="Output in CSV format")
    parser.add_argument(
        "--date",
        type=str,
        help="Specify the date in YYYY-MM-DD format (e.g., 2023-10-17)",
    )
    parser.add_argument(
        "--start-date",
        type=str,
        help="Specify the start date for the date range (YYYY-MM-DD)",
    )
    parser.add_argument(
        "--end-date",
        type=str,
        help="Specify the end date for the date range (YYYY-MM-DD)",
    )
    parser.add_argument(
        "--days",
        type=str,
        help="Specify an integer for the number of past days to retreive",
    )
    parser.add_argument(
        "--seconds", action="store_true", help="Output recorded time in seconds"
    )
    parser.add_argument(
        "-u",
        "--user",
        type=str,
        help="Specify the full or partial name to limit the results by (e.g., William)",
    )
    parser.add_argument("-v", action="store_true", help="Verbose output")
    args = parser.parse_args()

    if args.csv:
        csv = "true"
    else:
        csv = 0

    if args.v:
        print("Verbose logging enabled")
        debug = "true"
    else:
        debug = 0

    if args.seconds:
        seconds = "true"
    else:
        seconds = 0

    if args.start_date and args.end_date:
        try:
            start_date = datetime.strptime(args.start_date, "%Y-%m-%d")
            end_date = datetime.strptime(args.end_date, "%Y-%m-%d")
        except ValueError:
            print("Invalid date format. Please use YYYY-MM-DD format.")
            exit(1)
    elif args.date:
        try:
            specific_date = datetime.strptime(args.date, "%Y-%m-%d")
            start_date = specific_date
            end_date = specific_date
        except ValueError:
            print("Invalid date format. Please use YYYY-MM-DD format.")
            exit(1)
    elif args.days:
        try:
            days = int(args.days, base=0)
            start_date = datetime.now() - timedelta(days=days)
            end_date = datetime.now()
        except ValueError:
            print("Invalid days format. Days must be an integer.")
            exit(1)
    else:
        # Calculate the date for the previous business day (excluding weekends)
        today = datetime.now()
        last_business = today - timedelta(days=1)
        while not last_business.weekday():  # Skip weekends (Saturday and Sunday)
            last_business -= timedelta(days=1)
        start_date = last_business
        end_date = last_business
    if debug:
        if start_date == end_date:
            print(f"Getting entries from {start_date}")
        else:
            print(f"Getting entries from {start_date} through {end_date}")

    start_date_tmp = start_date - timedelta(days=1)
    start_date_str = start_date_tmp.strftime("%Y-%m-%d")
    end_date_tmp = datetime.now() + timedelta(days=1)
    end_date_str = end_date_tmp.strftime("%Y-%m-%d")

    # Create a JQL query for the date range and project
    jql_query = f"project = {project_key} AND updated >= {start_date_str} AND updated < {end_date_str}"

    if debug:
        print(f"JQL query: {jql_query}")

    # Set up the API request headers
    headers = {
        "Accept": "application/json",
    }

    # Set up the authentication
    auth = (username, api_token)

    # Send the request to Jira Cloud
    try:
        response = requests.get(
            "https://vtnis.atlassian.net/rest/api/3/search",
            params={
                "jql": jql_query,
                "fields": "worklog,summary",
                "maxResults": "2000",
            },
            headers=headers,
            auth=auth,
            timeout=120,
        )
    except requests.exceptions.Timeout:
        print("Timed out")

    if response.status_code == 200:
        data = response.json()

        # Create a list to store worklog entries
        worklogs = []

        # Parse the response and extract worklog details
        for issue in data.get("issues", []):
            issue_key = issue["key"]
            summary = issue["fields"]["summary"]

            for worklog in issue["fields"]["worklog"]["worklogs"]:
                started = worklog["started"][:10]
                created = worklog["created"][:10]
                updated = worklog["updated"][:10]
                work_started_date = datetime.strptime(started, "%Y-%m-%d").date()
                author_name = worklog["updateAuthor"]["displayName"]
                if debug:
                    print(
                        f"Checking if {work_started_date} >= {start_date.date()} and {work_started_date} <= {end_date.date()} for ticket {issue_key} - {author_name}")
                if (
                    work_started_date >= start_date.date()
                    and work_started_date <= end_date.date()
                ):
                    if debug:
                        print("true")
                    if seconds:
                        time_spent = worklog["timeSpentSeconds"]
                    else:
                        time_spent = worklog["timeSpent"]

                    # Filter for user if specified with user argument
                    if args.user and args.user.lower() not in author_name.lower():
                        break

                    worklogs.append(
                        {
                            "issue_key": issue_key,
                            "summary": summary,
                            "author": author_name,
                            "time_spent": time_spent,
                            "started": started,
                            "created": created,
                            "updated": updated,
                        }
                    )
                else:
                    if debug:
                        print("false")

        # Sort worklog entries by author's name
        worklogs.sort(key=lambda x: x["author"])

        # Print the sorted worklog entries
        for entry in worklogs:
            if csv:
                print(
                    f"{entry['author']},{entry['time_spent']},{entry['issue_key']},{entry['summary']}"
                )
            else:
                print(
                    f"{entry['author']} - {entry['time_spent']} - {entry['issue_key']} - {entry['summary']}"
                )
            if debug:
                print(
                    f"Started: {entry['started']} - Created: {entry['created']} - Updated: {entry['updated']}"
                )

    else:
        print(f"Failed to retrieve data. Status code: {response.status_code}")
        print(response.text)


if __name__ == "__main__":
    main()
